import java.util.*;
import java.util.regex.*;

/** Quaternions. Basic operations. */
public class Quaternion {

   // TODO!!! Your fields here!

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */

   private double a, b, c, d;


   public Quaternion (double a, double b, double c, double d) {
      // TODO!!! Your constructor here!
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return this.a;
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return this.b;
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return this.c;
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return this.d;
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   public Boolean isNegative(double d){
      if (d < 0){
         return true;
      }
      return false;
   }
   @Override
   public String toString() {
      String ans = "";
      ans += getRpart();
      if (isNegative(getIpart())) {
         ans += getIpart()+"i";
      }else{ans += "+"+getIpart()+"i";};
      if (isNegative(getJpart())) {
         ans += getJpart()+"j";
      }else{ans += "+"+getJpart()+"j";};
      if (isNegative(getKpart())) {
         ans += getKpart()+"k";
      }else{ans += "+"+getKpart()+"k";};

      return ans;
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      String[] sArray = s.split("\\+|(?=-)");
      for (String str: sArray) {
         int count = 0;
         for (int i = 0; i < str.length(); i++) {
            if (Character.isDigit(str.charAt(i))){
               count++;
            }
         }if (count < 1) {
            throw new RuntimeException("There should be number in "+s);
         }

      }


      if (sArray.length == 1) {
         if (sArray[0].contains("\\.")){
            throw new RuntimeException("Quaternion contains illegal symbols " + s);
         }
         return new Quaternion(Double.parseDouble(sArray[0]), 0, 0, 0);
      }
      if (sArray.length == 2) {
         if (!(sArray[1].contains("i"))){
            throw new RuntimeException("Quaternion doesn't count 'i' in  " + s);
         }
         int count = 0;
         for (int i=0, len = sArray[1].length(); i < len; i++){
            if (Character.isAlphabetic(sArray[1].charAt(i))){
               count ++;
            }
         }
         if (count > 1){
            throw new RuntimeException("Quaternion has too many letters " + s);
         }
         return new Quaternion(Double.parseDouble(sArray[0]), Double.parseDouble(sArray[1].replace("i", "")), 0, 0);
      }
      if (sArray.length == 3) {
         if (!(sArray[1].contains("i"))){
            throw new RuntimeException("Quaternion doesn't count 'i' in  " + s);
         }
         int count1 = 0;
         for (int i=0, len = sArray[1].length(); i < len; i++){
            if (Character.isAlphabetic(sArray[1].charAt(i))){
               count1 ++;
            }
         }
         if (count1 > 1){
            throw new RuntimeException("Quaternion has too many letters " + s);
         }
         if (!(sArray[2].contains("j"))){
            throw new RuntimeException("Quaternion doesn't count 'j' in " + s);
         }
         int count2 = 0;
         for (int i=0, len = sArray[2].length(); i < len; i++){
            if (Character.isAlphabetic(sArray[2].charAt(i))){
               count2 ++;
            }
         }
         if (count2 > 1){
            throw new RuntimeException("Quaternion has too many letters  " + s);
         }
         return new Quaternion(Double.parseDouble(sArray[0]), Double.parseDouble(sArray[1].replace("i", "")), Double.parseDouble(sArray[2].replace("j", "")), 0);
      }
      if (sArray.length == 4) {
         if (!(sArray[1].contains("i"))){
            throw new RuntimeException("Quaternion doesn't count 'i' in  " + s);
         }
         int count1 = 0;
         for (int i=0, len = sArray[1].length(); i < len; i++){
            if (Character.isAlphabetic(sArray[1].charAt(i))){
               count1 ++;
            }
         }
         if (count1 > 1){
            throw new RuntimeException("Quaternion has too many letters " + s);
         }
         if (!(sArray[2].contains("j"))){
            throw new RuntimeException("Quaternion doesn't count 'j' in " + s);
         }
         int count2 = 0;
         for (int i=0, len = sArray[2].length(); i < len; i++){
            if (Character.isAlphabetic(sArray[2].charAt(i))){
               count2 ++;
            }
         }
         if (count2 > 1){
            throw new RuntimeException("Quaternion has too many letters  " + s);
         }
         if (!sArray[3].contains("k")){
            throw new RuntimeException("Quaternion doesn't count 'k' in  " + s);
         }
         int count = 0;
         for (int i=0, len = sArray[3].length(); i < len; i++){
            if (Character.isAlphabetic(sArray[3].charAt(i))){
               count ++;
            }
         }
         if (count > 1){
            throw new RuntimeException("Quaternion has too many letters " + s);
         }
         return new Quaternion(
                 Double.parseDouble(sArray[0]),
                 Double.parseDouble(sArray[1].replace("i", "")),
                 Double.parseDouble(sArray[2].replace("j", "")),
                 Double.parseDouble(sArray[3].replace("k", "")));
      }
      return new Quaternion(0,0,0,0);
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(a, b, c, d);
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public static boolean doubleCompare(double x, double y) {
      return Math.abs(x - y) < 0.0001;
   }

   public boolean isZero(){
      return doubleCompare(0, this.a) && doubleCompare(0, this.b) && doubleCompare(0, c) && doubleCompare(0, this.d);
      }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(this.a, -this.b, -this.c, -this.d);
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-this.a, -this.b, -this.c, -this.d);
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      return new Quaternion(this.a+q.a, this.b+q.b, this.c+q.c, this.d+q.d);
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      try {
         double a1 = this.a;
         double b1 = this.b;
         double c1 = this.c;
         double d1 = this.d;
         double a2 = q.a;
         double b2 = q.b;
         double c2 = q.c;
         double d2 = q.d;

         double a = a1 * a2 - b1 * b2 - c1 * c2 - d1 * d2;
         double b = a1 * b2 + b1 * a2 + c1 * d2 - d1 * c2;
         double c = a1 * c2 - b1 * d2 + c1 * a2 + d1 * b2;
         double d = a1 * d2 + b1 * c2 - c1 * b2 + d1 * a2;

         return new Quaternion(a, b, c, d);
      } catch (RuntimeException e){
         throw new RuntimeException("Cant multiply - " + e);
      }
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      try {
         return new Quaternion(this.a * r, this.b * r, this.c * r, this.d * r);
      } catch (RuntimeException e){
         throw new RuntimeException("Cant multiply - " + e);
      }
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      if (this.isZero()){
         throw new RuntimeException("Can't divide with 0");
      }
      double x = this.a*this.a + this.b*this.b + this.c*this.c + this.d*this.d;
      return new Quaternion(this.a/x, (-this.b)/x, (-this.c)/x, (-this.d)/x);
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      try {
         return this.plus(q.opposite());
      } catch (IllegalArgumentException e){
         throw new IllegalArgumentException("Can't divide - " + e);
      }
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()){
         throw new RuntimeException("Can't divide with 0");
      }
      return this.times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if (q.isZero()){
         throw new RuntimeException("Can't divide with 0");
      }
      return q.inverse().times(this);
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if(qo instanceof Quaternion) {
         return doubleCompare(this.a, ((Quaternion) qo).getRpart()) && doubleCompare(this.b, ((Quaternion) qo).getIpart())
                 && doubleCompare(this.c, ((Quaternion) qo).getJpart()) && doubleCompare(this.d, ((Quaternion) qo).getKpart());
      }
      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      Quaternion x = this.times(q.conjugate());
      System.out.println(x.conjugate());
      x.plus(x).times(0.5);
      x.b = 0;
      x.c = 0;
      x.d = 0;
      return x;
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   // https://gist.github.com/mxrguspxrt/4044808
   @Override
   public int hashCode() {
      return new Double(this.a).hashCode()/ 9 * 79 + new Double(this.b).hashCode()/ 9 * 79 + new Double(this.c).hashCode()/ 9 * 79
              + new Double(this.d).hashCode()/ 9 * 79;
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(this.a*this.a+this.b*this.b+this.c*this.c+this.d*this.d);
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      Quaternion hash1 = new Quaternion (1., 2.,  3., 4.);
      Quaternion hash2 = new Quaternion (1., 3.,  2., 4.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("HASH: " + hash1.hashCode());
      System.out.println ("HASH: " + hash2.hashCode());
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
      System.out.println("VALUE:" + valueOf(arv2.toString()));
      System.out.println("VALUE:" + valueOf("1.0-2.0i-1.0j+2.0k"));
      System.out.println("VALUE:" + valueOf("1-2i-3j+2k"));
   }
}
// end of file
